//Minka adapter for payment gateways
//implements 4 endpoints
// /gateway/in - cash in
// /gateway/out - cash out
// /gateway/verify - verify bill
// /gateway/pay - pay bill

'use strict';

const Minka = require('./minka.js');

var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');

class gateway_adapter {

  constructor () {

    this.bill = undefined;
    this.subscription = undefined;
    this.minka = new Minka();

  }

  //cash-in method
  in(id) {

  }

  //cash-out method
  out(id) {

  }

  //verify bill method
  verify(bill) {

  }

  //pay bill/subscription method
  pay(source, cb) {
    //case pay bill

    if (!(JSON.parse(source).bill === undefined)) {
      var bill = JSON.parse(source).bill;
      console.log(`adapter pay() ${bill}`)
      var gateway = this;
        //pay bill
      this.payBill(bill, function(processed_bill){
          //if no error, create minka/blockchain transaction
          //prepare data for minka/blockchain transaction
          gateway.minka.payment_profile = new Object();
          gateway.minka.payment_profile.lukatag = bill.storeId;
          gateway.minka.profile = new Object();
          gateway.minka.profile.lukatag = bill.personId;
          gateway.minka.payment_currency = bill.currency;
          gateway.minka.payment_amount = bill.amount;
          gateway.minka.payment_note = `Has pagado servicio '${bill.description}' ${bill.amount} ${bill.currency}. Número de factura ${bill.billId}`;
          if (bill.error === undefined || bill.error == '') {
            /*gateway.minka.sendPayment(function(){
              //reference new txid
              processed_bill.txid = gateway.minka.payment_txid;

            })
            */
            cb(processed_bill);
          }
          else {
            cb(processed_bill);
          }

      })
    }
    //case pay subscription
    if (!(source.subscription === undefined)) {
        this.paySubscription(source, function(processed_bill){
          cb(processed_bill);
        })
    }
  }

  //PRIVATE functions
  prepopulateBill() {

    this.bill.billId = `${new Date().getTime()}-${Math.floor(Math.random()*1000)}`;
    var bill = this.bill;
    console.log(`prepoluate ${JSON.stringify(this.bill.serviceId)}`);
    var _service = this.minka.getService(this.bill.serviceId, function(service){

      bill.serviceName = service.name;
      bill.storeId = service.lukatag;
      bill.internalStoreId = service.internalStoreId;
      bill.internalServiceId = service.internalServiceId;
      bill.paymentGateway = service.paymentGateway;
      bill.description = service.description;
      bill.status = 'DRAFT';
      bill.created = new Date().toISOString();
      bill.error = '';
    });


  }

  payBill(bill, cb) {
    this.bill = bill;

    //check if it is a new bill. If TRUE then prepoluate it
    if (bill.status===undefined){
      this.prepopulateBill();
    }

    //switch case paymentGateway
    if (bill.paymentGateway == "SINTESIS") {
      //payment gateways
      const Adapter_sintesis = require('./gateway_adapter_sintesis.js');
      var adapter_sintesis = new Adapter_sintesis();
      //get gateway object and initialize gateway adapter
      this.minka.getGateway(this.bill.paymentGateway, function(gateway){
          adapter_sintesis.gateway = gateway;
          bill.status = "PENDING";
          adapter_sintesis.payBill(bill, function(processed_bill) {
            cb(processed_bill)
          });
      });
    }
  }

  paySubscription(subscription, cb) {
    cb(subscription);
  }

}

module.exports = gateway_adapter;
