//Minka adapter for payment gateways
//implements 4 endpoints
// /gateway/in - cash in
// /gateway/out - cash out
// /gateway/verify - verify bill
// /gateway/pay - pay bill

'use strict';
var request=require('request');
var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');
var session_id = '';

class adapter_sintesis {

  constructor () {

    //Minka API reference
    this.gateway = undefined;
    //sintesis session id

  }

  //initialize Sintesis session
  //session exists 30 minutes and has to be recheked at every call
  initializeSession(cb) {
    //post initsession to this.gateway.parameters.initializeSessionURL
    request({
      method: 'POST',
      url: `${this.gateway.parameters.initializeSessionURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "usuario": "${this.gateway.parameters.username}",
      	"password": "${this.gateway.parameters.password}",
      	"origenTransaccion": "${this.gateway.parameters.transactionSource}"
        }`
    }, function (error, response, body) {
      //create new session
      if (response.statusCode === 200) {
        session_id = JSON.parse(body).idOperativo;
        console.log(`New session ${session_id}`);
        cb(true)
      }
      else {
        console.log(`Error initializing Sintesis session. Error ${JSON.parse(body).response.codError}, ${JSON.parse(body).response.mensaje}`);
        cb(false)
      }
    });

  }

  verifySession(cb) {
    //if seesion_id empty initialize it
    if (session_id == '') {
      this.initializeSession(function(success){
        cb(success);
      })
    }
    //if session_id not empty check if it is active
    else {
      request(`${this.gateway.parameters.verifySessionURL}/${session_id}`, function (error, response, body) {
        //check status code
        if (response.statusCode === 200) {
          //session is still valid
          console.log(`Old session ${session_id}`);
          cb(true);
        }
        else {
          //session is invalid, initialize it again
          this.initializeSession(function(success){
            cb(success);
          })
        }
      });
    }
  }

  //cash-in method
  in(id) {

  }

  //cash-out method
  out(id) {

  }

  //verify bill method
  verify(bill) {

  }

  //pay method
  payBill(bill, cb) {
    //check session_id and re-initialize if expired
    var gateway = this;
    this.verifySession(function(success){
      if (success) {
        console.log(`verifySession success`);
        //get a list of pending bills for transactionId and date
        gateway.pendingBills(bill.accountId, bill.internalStoreId, bill.internalServiceId, function(items) {
          if (items !== false) {
            console.log(`pendingBills success`);
            //pay bill through Sintesis
            gateway.processPayBill(items, bill, function(success){

              if (success) {
                console.log(`processPayBill success`);
                bill.status = "PAID";
                bill.updated = new Date().toISOString();
                cb(bill);
              }
              else {
                console.log(`processPayBill error`);
                bill.error = `Error processing bill`;
                bill.status = "PENDING";
                bill.updated = new Date().toISOString();
                cb(bill);
              }
            });
          }
          else {
            bill.error = `Bill doesn't exist`;
            bill.status = "INVALID";
            bill.updated = new Date().toISOString();
            //console.log(`payment_gateway_sintesis ${bill}`)
            cb(bill);
          }
        });
      }
      else {
        bill.status = "PENDING";
        bill.updated = new Date().toISOString();
        //console.log(`payment_gateway_sintesis ${bill}`)
        cb(bill);
      }
    });

  }


  processPayBill(items, bill, cb){
    console.log(`processPayBill wait`);
    console.log(`{
      "idOperativo": "${session_id}",
      "nroOperacion": "${items.transactionId}",
      "fechaOperativa": "${items.transactionDate}",
      "codModulo": ${bill.internalStoreId},
      "cuenta": "${bill.accountId}",
      "servicio": ${bill.internalServiceId},

      "items": [
          {
              "nroItem": "${items.pendingBills[0].item[0].number}",
              "monto": ${bill.amount}
          }
      ]
      }`);
    request({
      method: 'POST',
      url: `${this.gateway.parameters.payBillURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "idOperativo": "${session_id}",
        "nroOperacion": "${items.transactionId}",
        "fechaOperativa": "${items.transactionDate}",
        "codModulo": ${bill.internalStoreId},
        "cuenta": "${bill.accountId}",
        "servicio": ${bill.internalServiceId},

        "items": [
            {
                "nroItem": "${items.pendingBills[0].item[0].number}",
                "monto": ${bill.amount}
            }
        ]
        }`
    }, function (error, response, body) {
      //create new session
      if (response.statusCode === 200) {
        console.log(`Bill paid on Sintesis, session_id = ${session_id}`);
        cb(true)
      }
      else {
        console.log(`Error initializing Sintesis session. Error ${JSON.parse(body).codError}, ${JSON.parse(body).mensaje}`);
        cb(false)
      }
    });

  }

  //pay subscription method
  paySubscription(subscription, cb) {
    subscription.status = "PAID";
    console.log(`payment_gateway ${subscription}`)
    cb(subscription);

  }

  //lit of pending items/bills
  pendingBills(accountId, storeId, serviceId, cb){
    console.log(`{      "idOperativo": "${session_id}",      "codModulo": "${storeId}",      "codCriterio": "${serviceId}",      "codigo": [        "${accountId}"      ]      }`);
    request({
      method: 'POST',
      url: `${this.gateway.parameters.getBillsURL}`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "idOperativo": "${session_id}",
        "codModulo": "${storeId}",
        "codCriterio": "${serviceId}",
        "codigo": [
        	"${accountId}"
        ]
        }`
    }, function (error, response, body) {
      //create new session
      if (response.statusCode === 200) {
        cb(JSON.parse(body));
      }
      else {
        console.log(`Error calling pendingBills().`);
        cb(false)
      }
    });
  }
}

module.exports = adapter_sintesis;
