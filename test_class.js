'use strict';



class Luka {

  constructor (lukatag) {

    //authenticated Luka user profile
    this.lukatag = '';
    this.luka_profile = undefined;
    this.luka_balance = undefined;

    //Luka profile to Send/Recive amount
    this.luka_payment_tag = '';
    this.luka_payment_profile = undefined;
    this.luka_payment_amount = 0;
    this.luka_payment_note = '';

    this.bill = new Object();
    this.bill.billId = new Date().getTime() + '' + Math.random()-1000;
    this.bill.serviceName = 'Yanbal Recarga';
    this.bill.personId = '@tom';
    this.bill.amount = '100';
    this.bill.currency = 'BOB';
    this.bill.status = 'DRAFT';

    this.bill.observations = new Object();
    this.bill.observations.paymentMethod = "PAYME";

  }
}
module.exports = Luka;
const luka = new Luka();
luka.bill.name = '';
luka.bill.tax = new Object();
luka.bill.tax.vat = "10%";
console.log(JSON.stringify(luka.bill));
console.log();
