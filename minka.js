'use strict';
//Minka API middleware

var request=require('request');
var S = require('string');
const _ = require('underscore')
const i18n = require('i18n');

const minka_api_url = 'http://api.minka.io';
const minka_api_gateway_url = 'http://lukafbchatdev.herokuapp.com';

class Luka {

  constructor () {

    //FB sender id
    this.sender_id = '';

    this.error = '';

    //authenticated Luka user profile
    this.lukatag = '';
    this.profile = undefined;
    this.balance = undefined;

    //Luka profile to Send/Recive amount
    this.payment_profile = undefined;
    this.payment_amount = 0;
    this.payment_currency = "LUK";
    this.payment_note = '';

    this.payment_service_id = '';
    this.payment_account_id = '';

    //Luka service User account number
    this.payer_account_number = '';

    //Luka txid
    this.payment_txid = '';
    //Luka transaction verify code
    this.payment_passcode = '';
    //Luka login verify code
    this.login_passcode = '';
    //number of login attempts
    this.login_attempt = 1;

    //FB page link
    this.chatbot_FB_url = 'http://bit.ly/2pbWKCm';

    //FB bot object
    this.bot = undefined;

    //states
    this.previous_step = -1;
    this.current_step = 0;

  }

  //login function
  login (lukatag, cb) {

    var luka = this;
    request(`${minka_api_url}/person/${lukatag}`, function (error, response, body) {
      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        //logged in, get the profile
        luka.profile = JSON.parse(body);
        luka.lukatag = lukatag;
        cb(true);
      }
    });
  };

  //get receive/send person/merchant object
  initPayment (lukatag, cb) {
    var luka = this;
    lukatag = S(lukatag).toLowerCase();
    if (S(lukatag).startsWith('@')) lukatag = S(lukatag).toLowerCase().substr(1,lukatag.length);

    request(`${minka_api_url}/person/${lukatag}`, function (error, response, body) {
      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        //logged in, get the profile
        luka.payment_profile = JSON.parse(body);
        cb(true);
      }
    });
  };

  //getBalance function
  getBalance (cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;
    request(`${minka_api_url}/person/${this.lukatag}/balance`, function (error, response, body) {

      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        luka.balance = JSON.parse(body);
        cb(true);

      }
    });

  };

    //initiates a transfer; creates a Minka transaction
    //trasnaction is sent from luka.profile -> luka.payment_profile
    sendPayment(cb) {

      var luka = this;

      console.log(`{
        "data": {
               "_links": {
                   "destination": "${luka.payment_profile.lukatag}",
                   "source": "${luka.profile.lukatag}"
               },
               "amount": {
                   "currency": "${luka.payment_currency}",
                   "value": "${luka.payment_amount}"
               },
               "metadata": {
                   "description": "${luka.payment_note}"
               }
        }
    }`);

      request({
        method: 'POST',
        url: `${minka_api_url}/transfer`,
        headers: {
          'Content-Type': 'application/json'
        },
        body: `{
        	"data": {
                 "_links": {
                     "destination": "${luka.payment_profile.lukatag}",
                     "source": "${luka.profile.lukatag}"
                 },
                 "amount": {
                     "currency": "${luka.payment_currency}",
                     "value": "${luka.payment_amount}"
                 },
                 "metadata": {
                     "description": "${luka.payment_note}"
                 }
        	}
      }`
      }, function (error, response, body) {
        if (error) console.log(error);
        console.log(JSON.stringify(response));
        body = JSON.parse(body);

        luka.payment_txid = body._id;
        console.log(`Transaction ${body._id} created.`);

        cb(true)

      });
  };

  //initiates a transfer; creates a Minka transaction
  //trasnaction is sent from luka.payment_profile -> luka.profile
  receivePayment(cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);
    var luka = this;

    request({
      method: 'POST',
      url: `${minka_api_url}/transfer`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{
        "_links": {
            "source": "${luka.payment_profile.lukatag}",
            "destination": "${luka.profile.lukatag}"
        },
        "amount": {
            "currency": "${luka.payment_currency}",
            "value": "${luka.payment_amount}"
        },
        "metadata": {
            "description": "${luka.payment_note}"
        }
    }`
    }, function (error, response, body) {
      if (error) console.log(error);
      body = JSON.parse(body);
      luka.payment_txid = body.doc._id;
      console.log(`Transaction ${body.doc._id} created.`);

      cb(true)
    });
  };

  //verifies current transaction
  verifyTransaction(cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    request({
      method: 'POST',
      url: `${minka_api_url}/transfer/verify`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{ "id" : "${luka.payment_txid}", "code" : "${luka.payment_passcode}" }`
    }, function (error, response, body) {
      if (response.statusCode == 200) {
        cb(true)
      }
      else {
        cb(false)
      }
    });
  }

  //verifies PIN to SMS
  verifyPIN(cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);
    //FIX
    cb(true);
    return;
    var luka = this;

    request({
      method: 'POST',
      url: `${minka_api_url}/channel/sms/pin/verify`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{ "phone" : "${luka.profile.phone}", "code" : "${luka.login_passcode}" }`
    }, function (error, response, body) {
      //console.log(`body ${JSON.parse(body)} body.response ${JSON.parse(body.response)}`)
      if (JSON.parse(body).response == true) {
        cb(true)
      }
      else {
        cb(false)
      }
    });
  }

  //send a verification PIN using SMS
  sendPIN(cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    request({
      method: 'POST',
      url: `${minka_api_url}/channel/sms/pin`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{ "phone" : "${luka.profile.phone}" }`
    }, function (error, response, body) {
      cb(true)
    });
  }

  //send a message to the user using SMS
  sendSmsMessage(to_phone,message_body,cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    request({
      method: 'POST',
      url: `${minka_api_url}/channel/sms/text`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: `{ "phone" : "${to_phone}", "message" : "${message_body}" }`
    }, function (error, response, body) {
      if (error) console.log(error);
      cb(true)
    });
  }

  //get a list of all transactions
  getTransactions(cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    request(`${minka_api_url}/person/${luka.lukatag}/transfer`, function (error, response, body) {
      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        //logged in, get the profile
        console.log(`getTransaction() array ${luka_transfers} single ${luka_transfers} body ${body}`)
        var luka_transfers = JSON.parse(body);
        cb(luka_transfers);
      }
    });
  }

  //get a transactions
  getTransaction(txid,cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    request(`${minka_api_url}/transfer/${txid}`, function (error, response, body) {
      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        //return the transaction object
        var luka_transfers = JSON.parse(body);
        console.log(`getTransaction(txid) array ${luka_transfers} single ${luka_transfers} body ${body}`)
        cb(luka_transfers);
      }
    });
  }
  //get person profile
  getPerson(lukatag) {
    var luka = this;
    lukatag = S(lukatag).toLowerCase();
    var person = undefined;
    if (S(lukatag).startsWith('@')) lukatag = S(lukatag).toLowerCase().substr(1,lukatag.length);

    request(`${minka_api_url}/person/${lukatag}`, function (error, response, body) {
      //check status code
      if (response.statusCode !== 200) {
        cb(false);
      }
      else {
        //logged in, get the profile
        person = JSON.parse(body);
        cb(person);
      }
    });
  }

  //get a list of services
  getServices(filter,cb) {

    var luka = this;
    filter = S(filter).toLowerCase();
    var services = require('./_store_services.json')
    services = JSON.parse(JSON.stringify(_.filter(_.sortBy(services,'storeId'),function(row){return S(row.store).toLowerCase().startsWith(`${filter}`) || S(row.name).toLowerCase().startsWith(`${filter}`) || S(row.lukatag).toLowerCase().startsWith(`${filter}`) || S(row.country).toLowerCase().startsWith(`${filter}`)})));

    //console.log(`services ${JSON.stringify(services)} ${services.length}`)

    cb(services);

  }
  //get a service by serviceId
  getService(serviceId,cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    var services = require('./_store_services.json')
    services = JSON.parse(JSON.stringify(_.filter(_.sortBy(services,'storeId'),function(row){return row.serviceId==`${serviceId}`})));

    console.log(`services ${serviceId} ${JSON.stringify(services)} ${services.length}`)
    //this.biller_service = stores[0];
    if (services.length == 0)
      cb(false);
    else
      cb(services[0]);

  }
  //get a gateway by gatewayId
  getGateway(gatewayId,cb) {

    var luka = this;

    var gateway=require('./_gateways.json')
    gateway = JSON.parse(JSON.stringify(_.filter(_.sortBy(gateway,'_id'),function(row){return row.name==`${gatewayId}`})));

    console.log(`gateway ${JSON.stringify(gateway)} ${gateway.length}`)
    //this.biller_service = stores[0];
    if (gateway.length == 0)
      cb(false);
    else
      cb(gateway[0]);

  }
  //get a store by storeId
  getStore(storeId,cb) {

    var luka = this;

    var store= require('./_stores.json')
    store = JSON.parse(JSON.stringify(_.filter(_.sortBy(store,'storeId'),function(row){return row.lukatag==`${storeId}`})));

    console.log(`store ${JSON.stringify(store)} ${store.length}`)
    //this.biller_service = stores[0];
    if (store.length == 0)
      cb(false);
    else
      cb(store);

  }
  //verify accountID for serviceId
  verifyService(accountId, serviceId, cb) {
    if (accountId == "68830388" && serviceId == "1")
      cb(true);
    else if (accountId == "71340318" && serviceId == "2")
      cb(true);
    else if (accountId == "75593511" && serviceId == "3")
      cb(true);
    else if (accountId == "78888088" && serviceId == "4")
      cb(true);
    else if (accountId == "70697131" && serviceId == "5")
      cb(true);
    else if (accountId == "70804486" && serviceId == "6")
      cb(true);
    else if (accountId == "26895" && serviceId == "7")
      cb(true);
    else if (accountId == "144084468" && serviceId == "8")
      cb(true);
    else cb(false);
  }
  //get a service by serviceId
  getSubsciption(accountId,cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    var subscriptions = require('./_store_service_subscription.json')
    subscriptions = JSON.parse(JSON.stringify(_.filter(_.sortBy(subscriptions,'storeId'),function(row){return row.accountId==`${accountId}`})));

    console.log(`subscriptions ${subscriptions.length}`)
    //this.biller_service = stores[0];
    if (subscriptions.length == 0)
      cb(false);
    else
      cb(subscriptions[0]);

  }
  //get a service by serviceId
  getSubscriptions(personId,cb) {
    //if the user is not logged in return false
    if (this.profile === undefined) cb(false);

    var luka = this;

    var subscriptions = require('./_store_service_subscription.json')
    subscriptions = JSON.parse(JSON.stringify(_.filter(_.sortBy(subscriptions,'storeId'),function(row){return row.personId==`${personId}`})));

    console.log(`person subscriptions ${JSON.stringify(subscriptions)} ${subscriptions.length} ${luka.lukatag} ${luka.profile.lukatag}`)
    //this.biller_service = stores[0];
    if (subscriptions.length == 0)
      cb(false);
    else
      cb(subscriptions);

  }
//******************************************************
  //state machine // navigation

  getCurrentStep() {
    return this.current_step;
  }

  setCurrentStep(step) {
    this.previous_step = this.current_step;
    this.current_step = step;
  }


  executeLukaAction (sender_id, cb) {
    console.log(`Current step is ${this.current_step}`);
    switch(this.current_step*1) {
      case 12:
        //case CANCEL
        this.current_step = 10;
        //var step_message = "Last command has been cancelled.";
        var step_message = "La opción anterior ha sido cancelada.";
        var _step_message = this.bot.sendMessage(sender_id, {'text': step_message});

      break;

      case 13:
        //case BALANCE
        this.current_step = 0;
        var luka = this;
        luka.getBalance(function(result) {

          //send a balance
          var luka_balance = "0";
          if (luka.balance != undefined) {
            luka_balance = `${luka.balance[0].qty} ${luka.balance[0].name}`;
          }

          //var step_message = `Your balance is ${luka.balance[0].qty} ${luka.balance[0].name}.`;
          var step_message = `Tu saldo es ${luka_balance}.`;
          var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
        });

      break;

      case 21:
        //case SEND_REQUEST
        //var step_message = "Type the @LukaTag to whom you'd like to send or request payment.";
        var step_message = "Escribe el  @LukaTag a quien quieres enviar o solicitar un pago.";
        var _step_message = this.bot.sendMessage(sender_id, {'text': step_message});
      break;
      case 22:
        //case SEND_REQUEST
        //var step_message = "Type the amount in LUK.";
        var step_message = "Escribe el valor en LUK	";
        var _step_message = this.bot.sendMessage(sender_id, {'text': step_message});
      break;
      case 23:
        //case SEND_REQUEST
        //var step_message = "Leave a note.";
        var step_message = "Adiciona una nota.";
        var _step_message = this.bot.sendMessage(sender_id, {'text': step_message});
      break;
      case 24:
        //case SEND_REQUEST
        var step_message = {
            "attachment":{
              "type":"template",
              "payload":{
                "template_type":"button",
                //"text":"Would you like to send or request?",
                "text":"Quieres enviar o solicitar?",
                "buttons":[
                  {
                    "type":"postback",
                    //"title":"Send",
                    "title":"Enviar",
                    "payload":"SEND_PAYMENT"
                  },
                  {
                    "type":"postback",
                    //"title":"Request",
                    "title":"Solicitar",
                    "payload":"REQUEST_PAYMENT"
                  },
                  {
                    "type":"postback",
                    //"title":"Cancel",
                    "title":"Cancelar",
                    "payload":"CANCEL"
                  }
                ]
              }

          }};
        var _step_message = this.bot.sendMessage(sender_id, step_message);
      break;
      /*case 250:/
        //case SEND_REQUEST SEND
        var luka = this;
        //send
        luka.sendPayment(function(result){
          console.log(`Send payment Transaction created is ${result}`);
          //send PIN in SMS
          luka.sendPIN(function(result){
            //send message
            //var step_message = `Hola Luka! sent you SMS message to ${luka.profile.phone} with a PIN to verify this transaction. Please type PIN from your SMS message.`;
            var step_message = `Hola Luka! te envio un mensaje SMS to ${luka.profile.phone} con el PIN de confirmación para esta transacción. Por favor escribe el mensaje SMS.`;
            var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});

          })
        });
      break;*/
      case 250:
        //verify current transaction

        var luka = this;

        luka.verifyTransaction(function(result){
          if (result) {
            //transaction PERFORMED
            luka.current_step = 10;
            //send info SMS to receiver
            //var _sms_message = luka.sendSmsMessage(luka.payment_profile.phone,`You received ${luka.payment_amount} ${luka.payment_currency} from ${luka.payment_profile.lukatag}! Note: '${luka.payment_note}' ChatBot ${luka.chatbot_FB_url}`, function(result){
            var _sms_message = luka.sendSmsMessage(luka.payment_profile.phone,`Recibiste ${luka.payment_amount} ${luka.payment_currency} de ${luka.payment_profile.lukatag}! Nota: '${luka.payment_note}' ChatBot ${luka.chatbot_FB_url}`, function(result){
              //send info FB message to sender
              //var step_message = `Succesfully sent to ${luka.payment_profile.lukatag} - ${luka.payment_profile.firstname} ${luka.payment_profile.lastname} ${luka.payment_amount} ${luka.payment_currency}`;
              var step_message = `Transacion exitoso ${luka.payment_profile.lukatag} - ${luka.payment_profile.firstname} ${luka.payment_profile.lastname} ${luka.payment_amount} ${luka.payment_currency}`;
              var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
            });
          }
          else {
            //not verified
            luka.current_step = 250;
            //var step_message = `Invalid PIN, please try again.`;
            var step_message = `La transacción no se ha verificado. ¡Inténtalo de nuevo!`;
            var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
          }
        });
      break;
      case 251:
        //case SEND_REQUEST REQUEST
        //request

        var luka = this;

        var _rec = luka.receivePayment(function(result){
          console.log(`Request payment Transaction created is ${result}`);
          //send PIN in SMS
          //var _sms = luka.sendSmsMessage(luka.payment_profile.phone,`You have received an request for payment of ${luka.payment_amount} ${luka.payment_currency} from ${luka.payment_profile.lukatag}! Note: '${luka.payment_note}' ChatBot ${luka.chatbot_FB_url}`,function(result){
          var _sms = luka.sendSmsMessage(luka.payment_profile.phone,`Has recibido una solicitud de pago por ${luka.payment_amount} ${luka.payment_currency} de ${luka.payment_profile.lukatag}! Nota:${luka.payment_note}' ChatBot ${luka.chatbot_FB_url}`,function(result){
            //send message
            //var step_message = `Payment request has been delivered to ${luka.payment_profile.lukatag}.`;
            var step_message = `La solicitud de pago ha sido entregada a ${luka.payment_profile.lukatag}.`;
            var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});

          })
        });

        //var step_message = `Order number ##\nCurrency LUK\nReceived`;
        /*var _step_message = this.sendMessage(sender_id, {
            "attachment":{
              "type":"template",
              "payload":{
                "template_type":"generic",
                "elements":[
                   {
                    "title":"Receipt of Request",
                    "image_url":"https://lukafbchatbot.herokuapp.com/img/luka_logo.png",
                    "subtitle":"From - To, 120 LUK"
                  }
                ]
              }
            }
          },(err) => {
          console.log(err);
        });*/
      break;

      case 30:
        //case Pending
        var luka = this;
        //get pending transactions and loop 5 of them
        luka.getTransactions(function(transactions){
          //transactions = JSON.parse(JSON.stringify(_.chain(_.where(_.sortBy(transactions,'created'),{status:'PENDING', _links:{source:'@tom'}})).reverse()));

          transactions = JSON.parse(JSON.stringify(_.chain(_.where(_.sortBy(transactions,'created'),function(row){return row.status=='PENDING' && row._links.source==`${luka.profile.lukatag}`})).reverse()));
          var i = 0;
          while (i<5 && transactions[i] != undefined) {
            var _step_message = luka.bot.sendMessage(sender_id,
              {
              "attachment": {
                  "type": "template",
                  "payload": {
                      "template_type": "generic",
                      "elements": [
                          {
                              //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                              "title": `Pagar a ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                              "subtitle": `${transactions[i].metadata.description}\nCreated on ${transactions[i].created}`,
                              "buttons": [
                                  {
                                      //"title": "Pay with Luka",
                                      "title": "Paga con Luka",
                                      "type": "postback",
                                      "payload": `PAY_PENDING_${transactions[i]._id}`
                                  }
                              ]
                          }
                      ]
                  }
              }
              },(err) => {
              console.log(err);
            });
            i++;
          }
        luka.current_step = 10;
      });
      break;
      case 31:
      //case SEND_REQUEST
      var luka = this;
      var step_message = {
          "attachment":{
            "type":"template",
            "payload":{
              "template_type":"button",
              //"text":"Please confirm the payment",
              "text":"Por favor confirma el pago",
              "buttons":[
                {
                  "type":"postback",
                  //"title":"Send",
                  "title":"Enviar",
                  "payload":"SEND_PAYMENT"
                },
                {
                  "type":"postback",
                  //"title":"Cancel",
                  "title":"Cancelar",
                  "payload":"CANCEL"
                }
              ]
            }

        }};
      var _step_message = luka.bot.sendMessage(sender_id, step_message);
      break;
      case 40:
        var luka = this;
        //case ADD_SERVICE
        //var step_message = "Type the phone number, name or @LukaTag of the service you are trying to add.";
        var step_message = "Escriba el nombre de la empresa o @LukaTag del servicio que quiere agregar.";
        var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
      break;
      case 41:
        var luka = this;
        //case ADD_SERVICE
        //var step_message = "Type your ID related to the service: account ID or customer ID. It is usually printed on the receipt. If you don't know it please contact the service provider. Type 'cancel' if you want to quit adding service.";
        var step_message = `Usted ha elegido un servicio ${luka.biller_service.store} ${luka.biller_service.name}`;
        var _step_message = luka.bot.sendMessage(sender_id, {'text': step_message}, function(){
          step_message = "Escriba su numero de identificación de usuario para poder suscribirlo.";
          _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
        });

      break;
      case 50:
        //case Servicios
        var luka = this;
        //get pending transactions and loop 5 of them
        luka.getServices('',function(subscriptions){
          //transactions = JSON.parse(JSON.stringify(_.chain(_.where(_.sortBy(transactions,'created'),{status:'PENDING', _links:{source:'@tom'}})).reverse()));

          var i = 0;
          for (i=0;i<subscriptions.length;i++) {
            if (subscriptions[i].serviceType == "PREPAID") {
              var _step_message = luka.bot.sendMessage(sender_id,
                {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [
                            {
                                //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                                "title": `${subscriptions[i].name}`,
                                "subtitle": `${subscriptions[i].country}\n${subscriptions[i].description}\n${subscriptions[i].serviceType}\n${subscriptions[i].serviceCurrency}`,
                                "buttons": [
                                    {
                                        //"title": "Pay with Luka",
                                        "title": "Recarga",
                                        "type": "postback",
                                        "payload": `PAY_SUBSCRIPTION`
                                    }
                                ]
                            }
                        ]
                    }
                }
                },(err) => {
                console.log(err);
              });
            }
            else {
              var _step_message = luka.bot.sendMessage(sender_id,
                {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [
                            {
                                //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                                "title": `${subscriptions[i].name}`,
                                "subtitle": `${subscriptions[i].country}\n${subscriptions[i].description}\n${subscriptions[i].serviceType}\n${subscriptions[i].serviceCurrency}`,
                                "buttons": [
                                    {
                                        //"title": "Pay with Luka",
                                        "title": "Pagar",
                                        "type": "postback",
                                        "payload": `PAY_SUBSCRIPTION`
                                    }
                                ]
                            }
                        ]
                    }
                }
                },(err) => {
                console.log(err);
              });

            }
          }
        luka.current_step = 10;
      });
      break;
      case 51:
        var luka = this;
        //case Pay service - enter amount
        //var step_message = "Type your ID related to the service: account ID or customer ID. It is usually printed on the receipt. If you don't know it please contact the service provider.";
        step_message = "Escriba la cantidad.";
        _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});

      break;
      case 52:
        var luka = this;
        //case Pay service - pay with Luka
        //var step_message = "Type your ID related to the service: account ID or customer ID. It is usually printed on the receipt. If you don't know it please contact the service provider.";
        step_message = "Suscripción pagada!";
        _step_message = luka.bot.sendMessage(sender_id,
          {'text': step_message}
        );

      break;
      case 53:
        //case Pay service - pay with Credit card
        //callback from external webview/page render
        var luka = this;
        //case Pay service

        //step_message = 'Credit card confirmed, now "Hola Luka!" is paying the bill! Please wait for the confirmation of the payment.';
        step_message = 'Tarjeta de crédito confirmada, ahora "Hola Luka!" está pagando la factura. Por favor espere la confirmación del pago.';
        _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});

        luka.current_step = 531;
        luka.executeLukaAction(sender_id);

      break;
      case 531:
        var luka = this;
        //case Pay service

        request({
          method: 'POST',
          url: `${minka_api_gateway_url}/gateway/pay`,
          headers: {
            'Content-Type': 'application/json'
          },
          body: `{
            	"bill":
            	{
            		"personId": "@tom",
            		"accountId": "144084468",
            		"serviceId": "8",
            		"amount": "100",
            		"currency": "BOB"

            	}
          }`
        }, function (error, response, body) {
          if (error) console.log(error);
          console.log(`${response} ${body}`);
          var bill = JSON.parse(body);

          console.log(`bill_id ${bill.billId} status ${bill.status}`);

          if (bill.status == 'PAID') {
            step_message = `Facture ${bill.billId} ${bill.amount} ${bill.currency} para '${bill.description}' pagada!`;
            _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
          }
          else {
            if (bill.error === undefined) bill.error = new Object();
            step_message = `Facture ${bill.billId} ${bill.amount} ${bill.currency} para '${bill.description}' NO pagada :( El estado ${bill.status}. ${bill.error}. `;
            _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});

          }
        });


      break;
      case 54:
        var luka = this;
        //case Pay service - rejected
        //var step_message = `Payment was not succesful :( ${luka.bill.error}`;
        var step_message = `El pago no tuvo éxito :(`;
        _step_message = luka.bot.sendMessage(sender_id, {'text': step_message});
      break;

    }
  }

}
module.exports = Luka;
