'use strict'
const url = require('url')
const qs = require('querystring')
const EventEmitter = require('events').EventEmitter
const request = require('request')
const crypto = require('crypto')
const Luka = require('./minka.js');
var S = require('string');
const i18n = require('i18n');
const _ = require('underscore')

//interval to resend reminder message
const REMINDER_MESSAGE_TIMER = 120 * 1000; //miliseconds into seconds = *1000
//store for interval function
var reminder_event, reminder_object, reminder_timestamp;

//lukatag param for minka object
var lukatag = '';
let luka = new Luka();

class Bot extends EventEmitter {
  constructor (opts) {
    super()

    opts = opts || {}
    if (!opts.token) {
      throw new Error('Missing page token. See FB documentation for details: https://developers.facebook.com/docs/messenger-platform/quickstart')
    }
    this.token = opts.token
    this.app_secret = opts.app_secret || false
    this.verify_token = opts.verify || false
  }

  getLukaProfile() {
    return lukatag;
  }

  setLukaProfile(l_obj) {
    luka = l_obj;
  }

  getProfile (id, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'GET',
      uri: `https://graph.facebook.com/v2.6/${id}`,
      qs: {
        fields: 'first_name,last_name,profile_pic,locale,timezone,gender',
        access_token: this.token
      },
      json: true
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)
      cb(null, body)
    })
  }

  sendMessage (recipient, payload, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'POST',
      uri: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {
        access_token: this.token
      },
      json: {
        recipient: { id: recipient },
        message: payload
      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  sendSenderAction (recipient, senderAction, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'POST',
      uri: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {
        access_token: this.token
      },
      json: {
        recipient: {
          id: recipient
        },
        sender_action: senderAction
      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  setGreetingMessage (message, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'POST',
      uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
      qs: {
        access_token: this.token
      },
      json: {
        setting_type: 'greeting',
        greeting:
          {
            'text': message
          }

      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  removeGreetingMessage (cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'DELETE',
      uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
      qs: {
        access_token: this.token
      },
      json: {
        setting_type: 'greeting'
      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  setThreadSettings (threadState, callToActions, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'POST',
      uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
      //uri: 'https://graph.facebook.com/v2.6/me/messenger_profile',
      qs: {
        access_token: this.token
      },
      json: {
        setting_type: 'call_to_actions',
        thread_state: threadState,
        call_to_actions: callToActions
      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  removeThreadSettings (threadState, cb) {
    if (!cb) cb = Function.prototype

    request({
      method: 'DELETE',
      uri: 'https://graph.facebook.com/v2.6/me/thread_settings',
      //uri: 'https://graph.facebook.com/v2.6/me/messenger_profile',
      qs: {
        access_token: this.token
      },
      json: {
        setting_type: 'call_to_actions',
        thread_state: threadState
      }
    }, (err, res, body) => {
      if (err) return cb(err)
      if (body.error) return cb(body.error)

      cb(null, body)
    })
  }

  setGetStartedButton (payload, cb) {
    if (!cb) cb = Function.prototype

    return this.setThreadSettings('new_thread', payload, cb)
  }

  setPersistentMenu (payload, cb) {
    if (!cb) cb = Function.prototype

    return this.setThreadSettings('existing_thread', payload, cb)
  }

  removeGetStartedButton (cb) {
    if (!cb) cb = Function.prototype

    return this.removeThreadSettings('existing_thread', cb)
  }

  removePersistentMenu (cb) {
    if (!cb) cb = Function.prototype

    return this.removeThreadSettings('existing_thread', cb)
  }



  middleware () {
    return (req, res) => {
      // we always write 200, otherwise facebook will keep retrying the request
      res.writeHead(200, { 'Content-Type': 'application/json' })
      if (req.url === '/_status') return res.end(JSON.stringify({status: 'ok'}))
      if (this.verify_token && req.method === 'GET') return this._verify(req, res)
      if (req.method !== 'POST') return res.end()

      let body = ''

      req.on('data', (chunk) => {
        body += chunk
      })

      req.on('end', () => {
        // check message integrity
        if (this.app_secret) {
          let hmac = crypto.createHmac('sha1', this.app_secret)
          hmac.update(body)

          if (req.headers['x-hub-signature'] !== `sha1=${hmac.digest('hex')}`) {
            this.emit('error', new Error('Message integrity check failed'))
            return res.end(JSON.stringify({status: 'not ok', error: 'Message integrity check failed'}))
          }
        }

        let parsed = JSON.parse(body)
        this._handleMessage(parsed)

        res.end(JSON.stringify({status: 'ok'}))
      })
    }
  }

  _handleMessage (json) {
    let entries = json.entry

    entries.forEach((entry) => {
      let events = entry.messaging

      events.forEach((event) => {
        // handle inbound messages and echos
        if (event.message) {
          if (event.message.is_echo) {
            this._handleEvent('echo', event)
          } else {
            this._handleEvent('message', event)
          }
        }

        //handle new chat thread
        //set introduction message
        //initiate message reminder
        if(event.postback && event.postback.payload === 'GETSTARTED' )
          {

            //send introduction message
            //let introductionMessage = "Hi, I'm Luka Bot, an automated assistant, to help you send payments, check your balance & see pending payments.";
            let introductionMessage = "Hola, soy Lukabot, un assitente automatico, te ayudare a realizar tus pagos, ver tu saldo & ver tus pagos pendientes.";

            this.sendMessage(event.sender.id, {'text': introductionMessage}, (err) => {
              if (err) console.log(err)
              console.log(`Echoed back : ${introductionMessage}`)

              //check if the user account is linked. If yes, no need to provide Luka login
              //let loginMessage = "Please provide your lukatag to begin.";
              let loginMessage = "Por favr ingresa tu @Lukatag para comenzar.";
              let _login = this.sendMessage(event.sender.id, {'text': loginMessage});

              //append the reminder message
              //create interval to call reminder messege every n seconds if no user activity is tracked

              /*
              reminder_event = event;
              reminder_object = this;

              setTimeout(function(){
                //let reminderMessage = "Here are some common commands you can type at anytime: help, balance, send & pending";
                let reminderMessage = "Aqui tienes los comandos generales que puedes ingresar en cualquer momento: ayuda, saldo, enviar, adicionar, servicios & pendiente";
                reminder_object.sendMessage(reminder_event.sender.id, {'text': reminderMessage}, (err) => {
                  if (err) throw err
                  console.log(`Here are some common commands you can type at anytime: help, balance, send & pending`)
                })
              }, REMINDER_MESSAGE_TIMER);
              */
            });
          }
          //handle Send/Request
        if(event.postback && event.postback.payload === 'SEND_REQUEST' )
          {
              luka.current_step = 21;
              luka.executeLukaAction(event.sender.id);
          }
          //handle Balance
        if(event.postback && event.postback.payload === 'BALANCE' )
          {
              luka.current_step = 13;
              luka.executeLukaAction(event.sender.id);
          }
          //handle Pending
        if(event.postback && event.postback.payload === 'PENDING' )
          {
              luka.current_step = 30;
              luka.executeLukaAction(event.sender.id);
          }
          //handle Pay Pending
        if(event.postback && S(event.postback.payload).startsWith('PAY_PENDING_') )
          {
              var bot = this;
            //parse txid from payload PAY_PENDING_txid
              var pos = event.postback.payload.lastIndexOf('_');
              luka.payment_txid = event.postback.payload.substring(pos+1,event.postback.payload.length);
              console.log('txid ' + luka.payment_txid)
              //get transaction destination and amount
              luka.getTransaction(luka.payment_txid,function(transaction){
                //transactions = JSON.parse(JSON.stringify(_.where(transactions,{_id:`${luka.payment_txid}`})));
                luka.payment_amount = transaction.amount.value;
                console.log('amount ' + luka.payment_amount)
                console.log('target ' + transaction._links.destination)
                luka.initPayment(transaction._links.destination,function(result){
                  luka.current_step = 31;
                  luka.executeLukaAction(event.sender.id);

                });

              })
          }
          //handle Add Service
        if(event.postback && event.postback.payload === 'ADD_SERVICE' )
          {
              luka.current_step = 40;
              luka.executeLukaAction(event.sender.id);
          }
          //handle Add service
        if(event.postback && S(event.postback.payload).startsWith('ADD_SERVICE_') )
          {
              var bot = this;
            //parse txid from payload PAY_PENDING_txid
              var pos = event.postback.payload.lastIndexOf('_');
              var serviceId = event.postback.payload.substring(pos+1,event.postback.payload.length);
              console.log(`serviceId = ${serviceId}`)
              luka.getService(serviceId, function(result) {

                if (result == false) {
                  //invalid service lukatag/name/phone
                  //reply({ text: `We don't recognize ${service_filter} service. Please try to enter different @LukaTag, phone or name.` });
                  reply({ text: `No reconocemos el servicio ${service_filter}. Intenta ingresar diferentes @LukaTag, teléfono o nombre` });
                  console.log('Unrecognized service');
                }
                else {
                  luka.biller_service = result;
                  luka.current_step = 41;
                  luka.executeLukaAction(event.sender.id);
                }
              });
          }
          //handle Services
        if(event.postback && event.postback.payload === 'SERVICES' )
          {
              luka.current_step = 50;
              luka.executeLukaAction(event.sender.id);
          }

          //handle Pay Service
        if(event.postback && event.postback.payload === 'PAY_SUBSCRIPTION' )
          {
              luka.current_step = 51;
              luka.executeLukaAction(event.sender.id);
          }

          //handle Pay with luka
        if(event.postback && event.postback.payload === 'PAY_SUBSCRIPTION_LUK' )
          {
              luka.current_step = 52;
              luka.executeLukaAction(event.sender.id);
          }

          //handle Pay with credit card
        if(event.postback && event.postback.payload === 'PAY_SUBSCRIPTION_CC' )
          {
              luka.current_step = 53;
              luka.executeLukaAction(event.sender.id);
          }

        if(event.postback && event.postback.payload === 'CANCEL' )
          {
              luka.current_step = 12;
              luka.executeLukaAction(event.sender.id);
          }
        if(event.postback && event.postback.payload === 'SEND_PAYMENT' )
          {
              luka.current_step = 250;
              luka.executeLukaAction(event.sender.id);
          }
        if(event.postback && event.postback.payload === 'REQUEST_PAYMENT' )
          {
              luka.current_step = 251;
              luka.executeLukaAction(event.sender.id);
          }

        // handle postbacks
        if (event.postback) {
          this._handleEvent('postback', event)
        }

        // handle message delivered
        if (event.delivery) {
          this._handleEvent('delivery', event)
        }

        // handle message read
        if (event.read) {
          this._handleEvent('read', event)
        }

        // handle authentication
        if (event.optin) {
          this._handleEvent('authentication', event)
        }

        // handle account_linking
        if (event.account_linking && event.account_linking.status) {
          if (event.account_linking.status === 'linked') {
            this._handleEvent('accountLinked', event)
          } else if (event.account_linking.status === 'unlinked') {
            this._handleEvent('accountUnlinked', event)
          }
        }
      })
    })
  }

  _getActionsObject (event) {
    return {
      setTyping: (typingState, cb) => {
        let senderTypingAction = typingState ? 'typing_on' : 'typing_off'
        this.sendSenderAction(event.sender.id, senderTypingAction, cb)
      },
      markRead: this.sendSenderAction.bind(this, event.sender.id, 'mark_seen')
    }
  }

  _verify (req, res) {
    let query = qs.parse(url.parse(req.url).query)

    if (query['hub.verify_token'] === this.verify_token) {
      return res.end(query['hub.challenge'])
    }

    return res.end('Error, wrong validation token')
  }

  _handleEvent (type, event) {
    this.emit(type, event, this.sendMessage.bind(this, event.sender.id), this._getActionsObject(event))
  }
}

module.exports = Bot
