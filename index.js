//
'use strict'
const http = require('http')
const express = require('express')
const bodyParser = require('body-parser')
var S = require('string');
//const Bot = require('./fb-messenger-bot.js')
const Bot = require('./fb-messenger-bot.js')
const Luka = require('./minka.js');

var lukatag = '';
let luka = new Luka();

const apiai = require('apiai');
const APIAI_TOKEN = "bb33a3cf597b4ce1bab56ca91ff8b916";
var apiaiApp = apiai(APIAI_TOKEN);
var apiaiRequest;

const sha1 = require('sha1');

//DEV
//const luka_web_app = 'https://lukafbchatdev.herokuapp.com';
//TEST
const luka_web_app = 'https://lukafbchatbot.herokuapp.com';


//DEVELOPMENT environment: http://lukafbchatdev.herokuapp.com/, https://www.facebook.com/Lukafbchatdev-253441458458830
/*
let bot = new Bot({
  token: 'EAADwewnfsXMBAFy5JayYxw3kBf23ZCSRdUClJjrxyEAHOlLZCVnk4CLYXu2ZC76Yo8OKfQ1Gw7RPKAbmGXuaesPuEXDCfKohVZATrZAdvnyO2Qt7E3uMhgMCYmZBwfKVsh9iDtElKEE2RP70lZAIBYBFxDX1LWZAnG5rVxM8Kk1PWwZDZD',
  verify: 'hail_hail_rocknroll',
  app_secret: '511ad02d3cd693af5113b63cfe5bd889'
})
*/
//TESTING environment: http://lukafbchatbot.herokuapp.com/, https://www.facebook.com/Lukafbchattest-1834638413464767

let bot = new Bot({
  token: 'EAADY9Vl9ETABAENXj9bUFo4pnhb8OvX9TCQc6ZBWIVuIZBUKTQa2xfbAPi2uWpssuiMmNHM055EZBK9XD73ZAYLzeQYZBbZCzx8ZCGoMWsOAOcvAQMneZAwDOIzPN7J7HlYyQoUO0rJWAgeich17lzWG8ZA1E8EAvAVPg1JdBwt2ElgZDZD',
  verify: 'hail_hail_rocknroll',
  app_secret: 'f595ca34449d6a1685c7c9e539cc9596'
})


var sessions = {};
var sessionId, session;

//session handler
function findOrCreateSession (fbid){
    var sessionId;
    // Let's see if we already have a session for the user fbid
    Object.keys(sessions).forEach(k => {
      if (sessions[k].fbid === fbid) {
         // Yep, got it!
         console.log('old session for ' + fbid);
         sessionId = k;
      }
    });
    if (!sessionId) {
      // No session found for user fbid, let's create a new one
      console.log('new session for ' + fbid);
      sessionId = new Date().toISOString();
      sessions[sessionId] = {fbid: fbid, context: {}};
    }
    return sessionId;
}

//verify session
function findSession (fbid){
    var sessionId;
    // Let's see if we already have a session for the user fbid
    Object.keys(sessions).forEach(k => {

      if (sessions[k].fbid == fbid) {
        return sessions[k];
      }
      else {
        return false;
      }
    });
}


//HELPER; used for development

bot.removeGetStartedButton(() => {
  console.log('Start button removed')
  bot.removePersistentMenu(() => {
    console.log('Persistant menu removed')
  })
})

//Setup a new bot thread
//set get started button
bot.setGetStartedButton([
    {
      "payload":"GETSTARTED"
    }
  ], () => {
  console.log('Start button appended')
  //set persistant menu
  let sb = bot.setPersistentMenu([
            {
              //"title":"Send/Request",
              "title":"Enviar/solicitar",
              "type":"postback",
              "payload":"SEND_REQUEST"
            },
            {
              //"title":"Pending",
              "title":"Pendiente",
              "type":"postback",
              "payload":"PENDING"
            },
            {
              //"title":"Add Service",
              "title":"Adicionar un servicio",
              "type":"postback",
              "payload":"ADD_SERVICE"
            }
          ], () => {
    console.log('Persistant menu appended')
    luka.setCurrentStep(0);

  })

})


bot.on('error', (err) => {
  console.log(err.message)
})




bot.on('message', (payload, reply) => {

  let sender = payload.sender.id;
  let text = payload.message.text
  //load facebook profile for engaded user
  bot.getProfile(sender, (err, profile) => {
    if (err) throw err

    //handle bot session for every user
    sessionId = findOrCreateSession(payload.sender.id);
    session = sessions[sessionId];
    if(session.context.luka == undefined) {
        session.context.luka =  new Luka();
        session.context.luka.sender_id = payload.sender.id;
    }
    luka = session.context.luka;

    //pass a reference to a bot
    luka.bot = bot;

    //is the user logged in Luka?
    if (luka.profile == undefined) {
      //user is not logged in
      //reset api.ai contexts
      var apiaiResetContext = apiaiApp.textRequest('resetContexts', {
          sessionId: sender,
          resetContexts: true
      });
      //take user message into lukatag and try to login
      lukatag = S(text).toLowerCase();
      if (S(text).startsWith('@')) lukatag = S(text).toLowerCase().substr(1,text.length);
      luka.login(lukatag, function(result) {
        console.log('luka profile  ' + luka.profile)
        console.log('fb profile is ' + profile + ' ' + JSON.stringify(profile));

        if (result == false) {
          //unsuccesful logon
          //invalid lukatag
          //text = "We don't recognize your Luka profile. Please type your lukatag to begin.";
          text = "No puedo reconocer tu perfil de Luka, por favor escribe tu @Lukatag para empezar.";
          reply({ text }, (err) => {
            if (err) throw err
            console.log(`Echoed back to ${profile.first_name} ${profile.last_name}: ${text}`)
          })
        }
        else {
          //valid lukatag, now we need a PIN/password
          luka.setCurrentStep(1);
          //send PIN in SMS
          luka.sendPIN(function(result){
            //send message
            //reply({ text: `Hola Luka! sent you SMS message to ${luka.profile.phone} with a PIN to verify this login. Please type PIN from your SMS message.`});
            reply({ text: `Hola Luka! te envio un mensaje SMS to ${luka.profile.phone} con el PIN de confirmación para esta iniciar sesión. Por favor escribe el mensaje SMS.`});
          })
        }
      });
    }
    else if (luka.getCurrentStep() == 1 && luka.login_attempt < 3) {
      //we need a password/PIN to login user
      luka.login_passcode = text;

      //check the password/PIN
      luka.verifyPIN(function(result){
        if (result == false) {
          //invalid PIN
          //add attempts
          luka.login_attempt++;
          //reply({ text: `Invalid PIN, please try again.`});
          reply({ text: `PIN no válido, inténtalo de nuevo.`});
        }
        else {
          //valid PIN, succesful logon
          luka.setCurrentStep(10);

          //set luka profile to Bot
          bot.setLukaProfile(luka);

          //reset login attempts
          luka.login_attempt = 1;

          //refresh balance
          luka.getBalance(function(result){
            if (err) console.log(err)
            //send introduction message and a balance
            //check if there is a balance
            var luka_balance = "0";
            if (luka.balance != undefined && luka.balance[0] !== undefined) {
              luka_balance = `${luka.balance[0].qty} ${luka.balance[0].name}`;
            }
            else {
              luka_balance = `0`;
            }

            //text = `Welcome ${luka.profile.firstname} ${luka.profile.lastname} with a lukatag ${luka.profile.lukatag}! Your balance is ${luka_balance}. Type 'help' for command list.`;
            text = `Bievenido ${luka.profile.firstname} ${luka.profile.lastname} con un lukatag ${luka.profile.lukatag}! Tu saldo es ${luka_balance}. Escribir 'ayuda' para la lista de comandos.`;
            reply({ text }, (err) => {
              if (err) throw err
              //send quick start menu
              let _quick_start = reply({
                  "attachment":{
                    "type":"template",
                    "payload":{
                      "template_type":"button",
                      //"text":"What do you want to do next?",
                      "text":"Que quieres hacer ahora?",
                      "buttons":[
                        {
                          "type":"postback",
                          //"title":"Send",
                          "title":"Enviar",
                          "payload":"SEND_REQUEST"
                        },
                        {
                          "type":"postback",
                          //"title":"Services",
                          "title":"Servicios",
                          "payload":"SERVICES"
                        },
                        {
                          "type":"postback",
                          //"title":"Pending",
                          "title":"Pendiente",
                          "payload":"PENDING"
                        }
                      ]
                    }

                }}, (err) => {
                  if (err) console.log(err)
                  console.log(`Echoed back quick start buttons`)
                }
              );

            })
          });
        }
      });

    }
    else if (luka.getCurrentStep() == 1 && luka.login_attempt >= 3) {
      //too many login attempts, reset login
      luka.profile = undefined;
      luka.login_attempt = 1;
      luka.lukatag = '';
      luka.setCurrentStep(0);
      //reply({ text: `Too many invalid login attempts. Enter your @lukatag again.`});
      reply({ text: `Demasiados intentos de inicio de sesión no válidos. Escriba su @lukatag de nuevo.`});
    }
    else {
      //user is logged in
      //parse user messages

      //forward message to api.ai
      apiaiRequest = apiaiApp.textRequest(text, {
          sessionId: sender
      });

      apiaiRequest.on('response', function(response) {

        console.log(`apiai: ${response.result.resolvedQuery} ${response.result.action} ${response.result.actionIncomplete} ${response.result.metadata.intentName} ${JSON.stringify(response.result.parameters)} ${JSON.stringify(response.result.contexts)}`);

        //print all messages
        for (var nResponses = 0; nResponses < response.result.fulfillment.messages.length; nResponses++) {
          var apiResponseText = response.result.fulfillment.messages[nResponses].speech;
          //if api.ai recognized intent, return api.ai response
          if (response.result.action != "input.unknown") {
            reply({ text: apiResponseText }, function(){
                console.log(`apiai response: ${apiResponseText}`);
            });

          }
        }

      });

      apiaiRequest.on('error', function(error) {
          console.log(error);
      });

      apiaiRequest.end();


      console.log('step ' + luka.getCurrentStep());
      if (luka.getCurrentStep() == 21) {
        //check if user with that @LukaTag exists
        var luka_payment_tag = S(text).toLowerCase();
        if (S(text).startsWith('@')) luka_payment_tag = S(text).toLowerCase().substr(1,text.length);
        luka.initPayment(luka_payment_tag, function(result) {

        if (result == false) {
          //unsuccesful logon
          //invalid lukatag
          //reply({ text: `We don't recognize ${text} @LukaTag owner. Please try to enter different @LukaTag.` });
          reply({ text: `No reconozco al usuario del ${text} @LukaTag owner. Por favor intenta nuevamente, escribiendo un @LukaTag diferente.` });
          console.log('Unrecognized Send/Receive user');
        }
        else {

          luka.setCurrentStep('22');
          luka.executeLukaAction(payload.sender.id, (err) => {
            console.log('Go to step ' + luka.getCurrentStep());
          });
        }
      })
      }
      else if (luka.getCurrentStep() >= 20 && S(text).toLowerCase()=='cancelar') {
        luka.setCurrentStep('12');
        luka.executeLukaAction(payload.sender.id);
        console.log('Step ' + luka.getCurrentStep());

      }

      else if (luka.getCurrentStep() == 22) {
        //check if amount is numeric
        if (isNaN(text)) {
          //reply({ text: `Amount needs to be a number. Please type the amount in ${luka.payment_currency} again.` });
          reply({ text: `El valor debe ser un número. Por favor escribe el valor en ${luka.payment_currency} nuevamente.` });
        }
        else if (luka.balance[0].qty < text) {
          //reply({ text: `You don't have enough ${luka.payment_currency} in your balance :( Please type smaller amount in ${luka.payment_currency}.` });
          reply({ text: `No cuentas con suficientes ${luka.payment_currency} en tu saldo: Por favor escribe un valor inferior en ${luka.payment_currency}.` });
        }
        else {
          luka.payment_amount = text;
          luka.setCurrentStep('23');
          luka.executeLukaAction(payload.sender.id);
        }
        console.log('Step ' + luka.getCurrentStep());
      }
      else if (luka.getCurrentStep() == 23) {
        //take a note message
        luka.payment_note = text;
        reply({ text: `${luka.payment_profile.lukatag} - ${luka.payment_profile.firstname} ${luka.payment_profile.lastname} ${luka.payment_amount} ${luka.payment_currency}` });
        luka.setCurrentStep('24');
        luka.executeLukaAction(payload.sender.id);
        console.log('Step ' + luka.getCurrentStep());
      }
      else if (luka.getCurrentStep() == 40) {
        //check if service with that @LukaTag or name or phone exists
        var service_filter = S(text).toLowerCase();
        if (S(service_filter).startsWith('@')) service_filter = S(service_filter).toLowerCase().substr(1,service_filter.length);
        luka.getServices(service_filter, function(result) {

        if (result == false) {
          //invalid service lukatag/name/phone
          //reply({ text: `We don't recognize ${service_filter} service. Please try to enter different @LukaTag, phone or name.` });
          reply({ text: `No reconocemos el servicio ${service_filter}. Intenta ingresar diferentes @LukaTag, teléfono o nombre` });
          console.log('Unrecognized service');
        }
        else {
          //reply({ text: `${result.length}` });
          //display filtered services
          var x;
          for (x=0; x<result.length;x++) {
            let _add_service = reply({

              "attachment": {
                  "type": "template",
                  "payload": {
                      "template_type": "generic",
                      "elements": [
                          {
                              //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                              "title": `${result[x].name}`,
                              "subtitle": `${result[x].country}\n${result[x].description}\n${result[x].serviceType}\n${result[x].serviceCurrency}`,
                              "buttons": [
                                  {
                                      //"title": "Add",
                                      "title": "Adicionar",
                                      "type": "postback",
                                      "payload": `ADD_SERVICE_${result[x].serviceId}`
                                  }
                              ]
                          }
                      ]
                  }
              }

            });
          }
        }
      })
      }
      else if (luka.getCurrentStep() == 41) {
        //check if service with that @LukaTag or name or phone exists
        var service_filter = S(text).toLowerCase();

        luka.verifyService(service_filter, luka.biller_service.serviceId, function(result) {

        if (result == false) {
          //invalid serviceId
          //reply({ text: `Invalid user code. Try agin.` });
          reply({ text: `Código de usuario no válido. Inténtalo de nuevo.` });
          console.log('Unrecognized accountId');
        }
        else {
          //subscription created
          reply({ text: `Servicio suscrito. Tipo 'servicios' para una lista de suscripciones.` });
          let _quick_start = reply({

            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            //"title": `Paying to ${transactions[i]._links.destination} ${transactions[i].amount.value} ${transactions[i].amount.currency}`,
                            "title": `${luka.biller_service.name}`,
                            "subtitle": `${luka.biller_service.description}\n${luka.biller_service.serviceType} ${luka.biller_service.serviceAmountType}\n${luka.biller_service.serviceAmount} ${luka.biller_service.serviceCurrency}`,
                        }
                    ]
                }
            }

          });
          luka.setCurrentStep(10);
        }
      })
      }

      else if (luka.getCurrentStep() == 51) {
        //choose payment method
        let _payment_options = reply({
            "attachment":{
              "type":"template",
              "payload":{
                "template_type":"button",
                //"text":"What do you want to do next?",
                "text":"Que quieres hacer ahora?",
                "buttons":[
                  {
                    "type":"web_url",
                    //"title":"Send",
                    "title":"Pagar con tarjeta",
                    "url":`${luka_web_app}/card/verify/${luka.sender_id}/100/LUK`,
                    "webview_height_ratio": "compact",
                    "messenger_extensions": true,
                    "fallback_url": `${luka_web_app}/card/verify/${luka.sender_id}/100/LUK`                  },
                  {
                    "type":"postback",
                    //"title":"Services",
                    "title":"Pagar con Luka",
                    "payload":"PAY_SUBSCRIPTION_LUK"
                  },
                  {
                    "type":"postback",
                    //"title":"Pending",
                    "title":"Cancelar",
                    "payload":"CANCEL"
                  }
                ]
              }

          }}, (err) => {
            if (err) console.log(err)
            console.log(`Echoed back payment options`)
          }
        );
      }

      else {

      switch (text.toLowerCase()) {
        //Send & Receive receiver

        //case 'help':
        /*
        case 'ayuda':
          //reply({ text: 'Commands you can use are: help, balance, send, pending & bye' }, (err) => {
          reply({ text: 'Las opciones con las que cuentas son: ayuda, enviar, pendiente, adicionar, servicios & cancelar' }, (err) => {
            if (err) throw err
          })
        break;
        */
        //case 'bye':
        case 'adios':
          luka.profile = undefined;
          luka.login_attempt = 1;
          luka.setCurrentStep(0);
          //reply({ text: 'Bye! You are now logged out. Type your lukatag to begin.' }, (err) => {
          reply({ text: 'Adios! No estas conectado. Escribe tu lukatag para comenzar.' }, (err) => {
            if (err) throw err
          })
        break;
        //case 'send':
        case 'enviar':
          luka.setCurrentStep('21');
          luka.executeLukaAction(payload.sender.id);
          console.log('Step ' + luka.getCurrentStep());
        break;
        //case 'balance':
        case 'saldo':
          luka.setCurrentStep('13');
          luka.executeLukaAction(payload.sender.id);
          console.log('Step ' + luka.getCurrentStep());
        break;
        //case 'pending':
        case 'pendiente':
          luka.setCurrentStep('30');
          luka.executeLukaAction(payload.sender.id);
          console.log('Step ' + luka.getCurrentStep());
        break;
        case 'servicios':
          luka.setCurrentStep('50');
          luka.executeLukaAction(payload.sender.id);
          console.log('Step ' + luka.getCurrentStep());
        break;
        case 'adicionar':
          luka.setCurrentStep('40');
          luka.executeLukaAction(payload.sender.id);
          console.log('Step ' + luka.getCurrentStep());
        break;
        default:
          //text = "I don't recognize this command.";
          /*text = "No reconozco esta opción.";
          reply({ text }, (err) => {
            if (err) throw err
            console.log(`Echoed back to ${profile.first_name} ${profile.last_name}: ${text}`)
          })
          */
      }
    }
    }
  })
})



bot.on('authentication', (payload, reply, actions) => {
  console.log('authenticated')
  reply({ text: 'You are authenticated!'}, (err, info) => {})
})

let app = express()
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.set('view engine', 'ejs');


app.get('/', (req, res) => {
  return bot._verify(req, res)
})

app.get('/card/verify/:sessid/:amount/:currency', (req, res) => {
  var FBsessionId = req.params.sessid;
  var amount = req.params.amount;
  var currency = req.params.currency;

  res.render('pages/card', {FBsessionId: FBsessionId, amount: amount, currency: currency});
  //res.sendFile('cardDetails.html', {root: './public'});
})

app.get('/card/confirm/:sessid/:amount/:currency', (req, res) => {
  var FBsessionId = req.params.sessid;
  var amount = req.params.amount;
  var currency = req.params.currency;

  //find Luka session
  sessionId = findOrCreateSession(FBsessionId);
  session = sessions[sessionId];
  luka = session.context.luka;

  res.render('pages/confirm', {FBsessionId: FBsessionId, amount: amount, currency: currency});

  luka.setCurrentStep('53');
  luka.executeLukaAction(FBsessionId);

})

app.get('/card/reject/:sessid/:amount/:currency/:error', (req, res) => {
  var FBsessionId = req.params.sessid;
  var amount = req.params.amount;
  var currency = req.params.currency;
  var error = req.params.error;

  //find Luka session
  sessionId = findOrCreateSession(FBsessionId);
  session = sessions[sessionId];
  luka = session.context.luka;

  res.render('pages/reject', {FBsessionId: FBsessionId, amount: amount, currency: currency, error: error});

  luka.setCurrentStep('54');
  luka.executeLukaAction(FBsessionId);

})

app.post('/', (req, res) => {
  bot._handleMessage(req.body)
  res.end(JSON.stringify({status: 'ok'}))
})

//Temporary Minka API /gateway implementation
app.get('/gateway/service', (req, res) => {
  var minka = new Luka();
  minka.getServices('', function(services){
      res.end(JSON.stringify(services))
  })

})

app.post('/gateway/pay', (req, res) => {
  var Adapter = require('./gateway_adapter.js');
  var adapter = new Adapter();
  var bill = JSON.stringify(req.body);
  console.log(`req.body.bill ${bill}`)
  adapter.pay(bill, function(processed_bill) {
      console.log(`/gateway/pay/bill ${JSON.stringify(processed_bill)}`)
      res.end(`${JSON.stringify(processed_bill)}`)
  });

})

http.createServer(app).listen(process.env.PORT || 3000)
